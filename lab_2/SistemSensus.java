import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan rtemplate ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Dhipta Raditya Y, NPM 1706028644, Kelas ....., GitLab Account: dhipta_raditya
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();

		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();

		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = Integer.parseInt(input.nextLine());

		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = Integer.parseInt(input.nextLine());

		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi =Integer.parseInt( input.nextLine());

		System.out.print("Berat Tubuh (kg)       : ");
		double berat = Integer.parseInt(input.nextLine());

		System.out.print("Jumlah Anggota Keluarga: ");
		int makanan = Integer.parseInt(input.nextLine());

		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();

		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();

		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = Integer.parseInt(input.nextLine());

		System.out.println(nama);
		System.out.println(alamat);
		System.out.println(panjang);
		System.out.println(lebar);
		System.out.println(tinggi);
		System.out.println(berat);
		System.out.println(makanan);
		System.out.println(tanggalLahir);
		System.out.println(catatan);
		System.out.println(jumlahCetakan);



		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		double rasio = (berat * 100* 100 * 100) / (panjang * lebar * tinggi);

		for (int i=1;i<=jumlahCetakan;i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase
			penerima = penerima.toUpperCase();
			// TODO Periksa ada catatan atau tidak
			// if (catatan == "") catatan = "Tidak ada catatan tambahan";
			// else catatan = catatan;

			// System.out.println(catatan.length() + "tralala");

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK "+ penerima+"\n";
			hasil += "--------------------\n";
			hasil += nama + " - "+ alamat + "\n";
			hasil += "Lahir pada tanggal" + tanggalLahir + "\n";
			hasil += "Rasio Berat Per Volume     = " + String.valueOf(rasio) + "\n";
			if(catatan.length() != 0){
				hasil += "Catatan: "+ catatan;
			}else{
				hasil += "Tidak ada catatan tambahan";
			}
			System.out.println(hasil);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		boolean valid = true;
		if (panjang > 250 || panjang < 1) valid = false;
		if (lebar > 250 || lebar < 1) valid = false;
		if (tinggi > 250 || tinggi < 1) valid = false;
		if (berat <= 0 || berat > 150) valid = false;


		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = "";
		int kalkulasi = 0;

		kalkulasi = (panjang * lebar * tinggi);
		nomorKeluarga += nama.charAt(0);
		for (int i=0; i<nama.length();i++){
			char tmp = nama.charAt(i);
			kalkulasi += tmp;
			kalkulasi %= 10000;
		}
		String kalkulasiStr = String.valueOf(kalkulasi);
		nomorKeluarga += kalkulasiStr;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (50000) * (365) * (makanan);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String[] kelahiran = tanggalLahir.split("-");
		String tahun = kelahiran[2];
		int tahunLahir = Integer.parseInt(tahun); // lihat hint jika bingung
		int umur = (2018) - (tahunLahir);

		String apartemen = "";
		int batasanTeksas = 100000000;
		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		if(umur< 19){
			// PPMT Rotunda
			apartemen = "PPMT, Kabupaten Rotunda";
		}else{
			if(anggaran<=batasanTeksas){
				// Teksas Sastra
				apartemen = "Teksas, Kabupaten Sastra";
			}else{
				// Mares Margonda
				apartemen = "Mares, Kabupaten Margonda";
			}
		}




		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "";
		rekomendasi += "REKOMENDASI APARTEMEN\n";
		rekomendasi += "--------------------\n";
		rekomendasi += "MENGETAHUI: Identitas keluarga: "+ nama + " - " + nomorKeluarga+"\n";
		rekomendasi += "MENIMBANG: " + "Anggaran makanan tahunan: Rp " +anggaran +"\n";
		rekomendasi += "           " + "Umur kepala keluarga: " + umur+"\n";
		if(valid){
			rekomendasi += "MEMUTUSKAN: keluarga " + nama + "akan ditempatkan di:\n" + apartemen;
		}else{
			rekomendasi += "WARNING: Keluarga ini tidak perlu direlokasi";
		}
		System.out.println(rekomendasi);

		input.close();
	}
}