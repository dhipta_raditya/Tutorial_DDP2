import java.util.Scanner;
import java.lang.*;
public class Manusia {
	private String nama;
	private int umur, uang;
	private float kebahagiaan = 50;

	Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		// this.kebahagiaan = kebahagiaan;
	}
	Manusia(String nama, int umur){
		this(nama,umur,50000);
	}

	public void setNama(String nama){
		this.nama = nama;
	}
	public void setUmur(int umur){
		this.umur = umur;
	}
	public void setUang(int uang){
		this.uang = uang;
	}
	public void setKebahagiaan(float kebahagiaan){
		this.kebahagiaan = kebahagiaan;
	}

	public String getNama(){
		return this.nama;
	}
	public int getUmur(){
		return this.umur;
	}
	public int getUang(){
		return this.uang;
	}
	public float getKebahagiaan(){
		return this.kebahagiaan;
	}

	public void beriUang(Manusia penerima){
		int total = 0;
		String kata;
		kata = penerima.nama;
		for(int i=0;i<kata.length();i++){
			char huruf = kata.charAt(i);
			total += huruf;
		}
		total *= 100;
		if(this.uang > total){

			penerima.uang += total;
			this.uang -= total;
			float tmp = total;

			penerima.kebahagiaan += (tmp / 6000);
			penerima.kebahagiaan = Math.min(100, penerima.kebahagiaan);

			this.kebahagiaan += (tmp / 6000);
			this.kebahagiaan = Math.min(100, this.kebahagiaan);

			String out = String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D",this.nama,total,penerima.nama);
			System.out.println(out);
			return ;
		}
		String out = String.format("%s ingin memberikan uang sebanyak kepada %s namun tidak cukup uang :(",this.nama,penerima.nama);
		System.out.println(out);
		return ;
	}

	public void beriUang(Manusia penerima, int total){
		if(this.uang > total){

			penerima.uang += total;
			this.uang -= total;
			float tmp = total;

			penerima.kebahagiaan += (tmp / 6000);
			penerima.kebahagiaan = Math.min(100, penerima.kebahagiaan);

			this.kebahagiaan += (tmp / 6000);
			this.kebahagiaan = Math.min(100, this.kebahagiaan);

			String out = String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D",this.nama,total,penerima.nama);
			System.out.println(out);
			return ;
		}
		String out = String.format("%s ingin memberikan uang kepada %s namun tidak cukup uang :(",this.nama,penerima.nama);
		System.out.println(out);
		return ;
	}

	public void bekerja(int durasi, int bebanKerja){
		if(this.umur<18){
			String out = String.format("%s belum boleh bekerja karena masih dibawah umur D:",this.nama);
			System.out.println(out);
			return ;
		}
		int diterima;
		int bebanKerjaTotal = durasi * bebanKerja;
		if(bebanKerjaTotal <= this.kebahagiaan){
			this.kebahagiaan -= bebanKerjaTotal;
			diterima = bebanKerjaTotal * 10000;
			this.uang += (diterima);

			String out = String.format("%s bekerja full time, total pendapatan : %d",this.nama, diterima);
			System.out.println(out);
		}else{
			float durasiBaruFloat = this.kebahagiaan / bebanKerja;
			int durasiBaru = (int)durasiBaruFloat;
			bebanKerjaTotal = durasiBaru * bebanKerja;
			this.kebahagiaan -= bebanKerjaTotal;
			diterima = bebanKerjaTotal * 10000;
			this.uang += (diterima);

			String out = String.format("%s tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : %d",this.nama,diterima);
			System.out.println(out);
		}
	}

	public void rekreasi(String namaTempat){
		int biaya = 10000 * namaTempat.length();
		if(this.uang > biaya){
			this.uang -= biaya;
			this.kebahagiaan = Math.min(100, this.kebahagiaan + namaTempat.length());
			String out = String.format("%s berekreasi di %s, %s senang :)",this.nama, namaTempat, this.nama);
			System.out.println(out);
			return;
		}
		String out = String.format("%s tidak mempunyai cukup uang untuk berekreasi di %s :(",this.nama, namaTempat, this.nama);
		System.out.println(out);
		return;
	}

	public void sakit(String namaPenyakit){
		this.kebahagiaan = Math.max (0, this.kebahagiaan - namaPenyakit.length());
		String out = String.format("%s terkena penyakit %s :O",this.nama, namaPenyakit);
		System.out.println(out);
	}

	public String toString(){
		return String.format("Nama\t\t:%s\nUmur\t\t:%d\nUang\t\t:%d\nKebahagiaan\t:%.1f", this.nama, this.umur, this.uang, this.kebahagiaan);
	}

}