
/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers = new Number[5][5];
	private Number[] numberStates = new Number[101];
	private boolean isBingo = false;

	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		if(this.numberStates[num] ==  null)return "Kartu tidak memiliki angka " + num;
		if(this.numberStates[num].isChecked())return num + " sebelumnya sudah tersilang";

		int posX = this.numberStates[num].getX();
		int posY = this.numberStates[num].getY();

		this.numbers[posX][posY].setChecked(true);
		this.numberStates[num].setChecked(true);
		this.isBingo = this.checkSelesai();
		return num + " tersilang";
	}

	public String info(){
		String ret = "";
		for(int i=0;i<5;i++){
			ret+='|';
			for(int j=0;j<5;j++){
				ret += this.numbers[i][j].isChecked()?" X ": " "+this.numbers[i][j].getValue();
				ret+=" |";
			}
			if(i!=4)ret+= "\n";
		}
		return ret;
	}

	public void restart(){
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				this.numbers[i][j].setChecked(false);
			}
		}
		for(int i=0;i<=99;i++){
			if(this.numberStates[i] == null)continue;
			this.numberStates[i].setChecked(false);
		}
		System.out.println("Mulligan!");
	}

	public boolean checkSelesai(){
		boolean vld;

		for(int i=0;i<5;i++){
			vld = true;
			for(int j=0;j<5;j++){
				if(!this.numbers[i][j].isChecked()){
					vld = false;
					break;
				}
			}
			if(vld)return vld;
		}

		for(int j=0;j<5;j++){
			vld = true;
			for(int i=0;i<5;i++){
				if(!this.numbers[i][j].isChecked()){
					vld = false;
					break;
				}
			}
			if(vld)return vld;
		}


		vld = true;
		for(int i=0;i<5;i++){
			if(!this.numbers[i][i].isChecked()){
				vld = false;break;
			}
		}
		if(vld)return vld;

		vld = true;
		for(int i=0;i<5;i++){
			if(!this.numbers[i][4-i].isChecked()){
				vld = false;
				break;
			}
		}
		return vld;
	}
}
